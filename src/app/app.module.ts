import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { SingleComponent } from './single/single.component';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';

import { MoviesService } from './apimoviedb.service';


import { MovieImagePipe } from './movie-image.pipe';
import { LoadingComponent } from './shared/loading/loading.component';
import { MoviesComponent } from './home/movies/movies.component';
import { YoutubeComponent } from './shared/youtube/youtube.component';
import { TimeBrazilPipe } from './time-brazil.pipe';
import { BtnGenreComponent } from './shared/btn-genre/btn-genre.component';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    SearchComponent,
    SingleComponent,
    MovieImagePipe,
    LoadingComponent,
    MoviesComponent,
    YoutubeComponent,
    TimeBrazilPipe,
    BtnGenreComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule
  ],
  providers: [
    MoviesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
