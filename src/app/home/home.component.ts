import { Component, OnInit, Input } from '@angular/core';
import { MoviesService } from '../apimoviedb.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [ MoviesService ]
})
export class HomeComponent implements OnInit {

  movies: any[] = [];
  loading: boolean;


  constructor(private dbmovie: MoviesService) {

    this.loading = true;


    this.dbmovie.getMovies().subscribe( (data: any) => {
      console.log(data);
      this.movies = data;
      this.loading = false;
    });
  }

  ngOnInit() {
  }

}
