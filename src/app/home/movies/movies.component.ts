import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MoviesService } from 'src/app/apimoviedb.service';


@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent {

  @Input() itens: any[] = [];

  id: number;
  listgenres: any[] = [];

  constructor( private router: Router, private dbmovie: MoviesService ) {

    this.dbmovie.getGenre()
      .subscribe(genres => {
        this.listgenres = genres.genres;
      });
  }

  showMovie( item: any ) {
    const movieId = item.id;

    this.router.navigate([ '/single', movieId ]);
  }


}
