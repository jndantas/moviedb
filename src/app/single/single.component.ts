import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../apimoviedb.service';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.css']
})
export class SingleComponent {

  movie: any[] = [];
  video: string[] = [];

  constructor(private router: ActivatedRoute, private dbmovie: MoviesService) {

    this.router.params.subscribe(params => {

      this.dbmovie.getMovieId(params['id'])
        .subscribe(movie => {
          this.movie = movie;
        });

      this.dbmovie.getVideo(params['id'])
        .subscribe(video => {
          this.video = video.results;
        });
    });
  }

}
