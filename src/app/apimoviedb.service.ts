import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  private apikey = '96025e42a89790de07304bf7d4f36790';
  private urlMoviedb = 'https://api.themoviedb.org/3';


  movies: any[] = [];

  constructor(private http: HttpClient) {}

  getMovies() {
    return this.getQuery('/discover/movie?sort_by=popularity.desc');
  }

  searchMovie(text: string) {
    return this.getQuery(
      `/search/movie?query=${text}&sort_by=popularity.desc`
    ).pipe(map((searchedMovie: any) => (this.movies = searchedMovie)));
  }

  getQueryforMovies(query: string) {
    const url = `${this.urlMoviedb}${query}?api_key=${this.apikey}&language=pt-BR&callback=JSONP_CALLBACK`;

    return this.http.jsonp(url, '');
  }

  getMovieId(id: string) {
    return this.getQueryforMovies(`/movie/${id}`)
    .pipe(map((data: any) => data));
  }

  private getQuery(query: string) {
    const url = `${this.urlMoviedb}${query}&api_key=${this.apikey}&language=pt-BR`;
    return this.http.get(url).pipe(
      map((movies: any) => {
        if (movies.results) { return movies.results; } else { return movies; }
      })
    );
  }

  getVideo(id: string) {
    return this.getQueryVideo(`${id}`)
    .pipe(map((data: any) => data));
  }

  getQueryVideo(id: string) {
    const url = `${this.urlMoviedb}/movie/${id}/videos?api_key=${this.apikey}&language=pt-BR&callback=JSONP_CALLBACK`;

    return this.http.jsonp(url, '');
  }

  getGenre() {
    return this.getQueryforMovies('/genre/movie/list')
    .pipe(map((data: any) => data));
  }
}
