import { Component } from '@angular/core';
import { MoviesService } from '../apimoviedb.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  movies: any[] = [];
  loading: boolean;

  constructor(private dbmovie: MoviesService) { }


  search(text: string) {
    console.log(text);
    this.loading = true;
    this.dbmovie.searchMovie( text )
          .subscribe((data: any) => {
            console.log(data);
            this.movies = data;
            this.loading = false;

          });
  }

}
